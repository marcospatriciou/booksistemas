<p align="center">
  <a href="https://gitpoint.co/">
    <img alt="GitPoint" title="GitPoint" src="https://www.jadlog.com.br/jadlog/img/logo_home.png" width="450">
  </a>
</p>

<p align="center">
  BitBucket Corporativo.
</p>



<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Cadastros de Atendimentos

- [Evento de Coleta / Entrega AC0001](#evento)
- [Geração de Ocorrência AC0002](#geracao)
- [Motivo de Evento de Coleta / Entrada AC0003](#motivo-evento)
- [Motivo de Ocorrência AC0004](#motivo-ocorrencia)
- [Ocorrências Padrão EDI AC0005](#padrao-edi)
- [Feedback](#feedback)
- [Contribuidores](#contributors)
- [Build Process](#build-process)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Evento

[![Build Status](https://img.shields.io/travis/gitpoint/git-point.svg?style=flat-square)](https://travis-ci.org/gitpoint/git-point)
[![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?style=flat-square)](https://coveralls.io/github/gitpoint/git-point)
[![All Contributors](https://img.shields.io/badge/all_contributors-73-orange.svg?style=flat-square)](./CONTRIBUTORS.md)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)
[![Gitter chat](https://img.shields.io/badge/chat-on_gitter-008080.svg?style=flat-square)](https://gitter.im/git-point)


Esta rotina permite cadastrar os possíveis eventos ocorridos em uma coleta (ordem de coleta) ou em uma entrega (CTRC).

Esta rotina também controla a hora de início e fim do evento, registrando o tempo total transcorrido de cada evento. 

É possível parametrizar a abertura dessa rotina automaticamente na baixa da ordem de coleta e Conhecimento de Transporte pela rotina LOG2240 (Manutenção Parâmetros Sistema).



**Exemplos de utilização.**

Cadastrar o Evento de Coleta
 
1. Na tela Evento Coleta/Entrega, clique em Incluir
2. Selecione o tipo de documento que será gerado.
3. Selecione também a empresa Logix na qual o documento foi gerado.
4. Informe os demais parâmetros necessários para este processo.
5. Confira os dados e confirme.

## Geracao

[![Build Status](https://img.shields.io/travis/gitpoint/git-point.svg?style=flat-square)](https://travis-ci.org/gitpoint/git-point)
[![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?style=flat-square)](https://coveralls.io/github/gitpoint/git-point)
[![All Contributors](https://img.shields.io/badge/all_contributors-73-orange.svg?style=flat-square)](./CONTRIBUTORS.md)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)
[![Gitter chat](https://img.shields.io/badge/chat-on_gitter-008080.svg?style=flat-square)](https://gitter.im/git-point)

Esta rotina permite cadastrar as ocorrências ocorridas durante a coleta ou entrega das cargas.

A ocorrência pode ser registrada para uma única nota ou para toda uma viagem entre outras opções.

A partir das ocorrências cadastradas é possível realizar os controles por meio de motivos de ocorrência para referência posterior e textos de problema e solução.

## Motivo Evento

[![Build Status](https://img.shields.io/travis/gitpoint/git-point.svg?style=flat-square)](https://travis-ci.org/gitpoint/git-point)
[![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?style=flat-square)](https://coveralls.io/github/gitpoint/git-point)
[![All Contributors](https://img.shields.io/badge/all_contributors-73-orange.svg?style=flat-square)](./CONTRIBUTORS.md)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)
[![Gitter chat](https://img.shields.io/badge/chat-on_gitter-008080.svg?style=flat-square)](https://gitter.im/git-point)

Esta rotina permite cadastrar todos os motivos de evento de coleta/entrega que serão utilizados no momento do lançamento destes eventos pela rotina OMC0088 (Evento Coleta/Entrega).

## Motivo Ocorrência

[![Build Status](https://img.shields.io/travis/gitpoint/git-point.svg?style=flat-square)](https://travis-ci.org/gitpoint/git-point)
[![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?style=flat-square)](https://coveralls.io/github/gitpoint/git-point)
[![All Contributors](https://img.shields.io/badge/all_contributors-73-orange.svg?style=flat-square)](./CONTRIBUTORS.md)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)
[![Gitter chat](https://img.shields.io/badge/chat-on_gitter-008080.svg?style=flat-square)](https://gitter.im/git-point)

Esta rotina permite cadastrar os motivos de ocorrência que serão utilizados para o lançamento das ocorrências do Sistema, sendo que não será possível que uma ocorrência seja cadastrada sem o motivo que a originou.

O motivo de ocorrência possui uma classificação entre eventos/ocorrências, uma situação que define como a carga transportada se encontra, indicadores,
eventos de geração automática (pontos do Sistema no qual serão gerados automaticamente a ocorrência), além de possuir justificativas aplicáveis a determinado evento.

Outra funcionalidade é a ação que determinado motivo de ocorrência pode exercer sobre o Sistema, quando for lançada uma ocorrência (efetiva baixa, gera reentrega, gera devolução, envia e-mail ou oculta operadores).

## Padrao EDI

[![Build Status](https://img.shields.io/travis/gitpoint/git-point.svg?style=flat-square)](https://travis-ci.org/gitpoint/git-point)
[![Coveralls](https://img.shields.io/coveralls/github/gitpoint/git-point.svg?style=flat-square)](https://coveralls.io/github/gitpoint/git-point)
[![All Contributors](https://img.shields.io/badge/all_contributors-73-orange.svg?style=flat-square)](./CONTRIBUTORS.md)
[![PRs Welcome](https://img.shields.io/badge/PRs-welcome-brightgreen.svg?style=flat-square)](http://makeapullrequest.com)
[![Commitizen friendly](https://img.shields.io/badge/commitizen-friendly-brightgreen.svg?style=flat-square)](http://commitizen.github.io/cz-cli/)
[![Gitter chat](https://img.shields.io/badge/chat-on_gitter-008080.svg?style=flat-square)](https://gitter.im/git-point)

Essa rotina permite cadastrar os motivos de ocorrência padrão Eletronic Data Interchange EDI, ou seja, os códigos normalmente utilizados no padrão PROCEDA.

Caso a transportadora não utilize o conceito, basta cadastrar o de/para somente para o cliente genérico do Sistema parametrizado (Manutenção Parâmetros Sistema).

## Feedback

Sinta a vontade para nos envia seu feedback. Novas funcionalidades são sempre bem vindas e são motores de nossa inovação


## Contributors

Este projeto é mantido pelo departamento de tecnologia e sistemas JadLog Logisica.

## Build Process

- Follow the [React Native Guide](https://facebook.github.io/react-native/docs/getting-started.html) for getting started building a project with native code. **A Mac is required if you wish to develop for iOS.**
- Clone or download the repo
- `yarn` to install dependencies
- `yarn run link` to link react-native dependencies
- `yarn start:ios` to start the packager and run the app in the iOS simulator (`yarn start:ios:logger` will boot the application with [redux-logger](<https://github.com/evgenyrodionov/redux-logger>))
- `yarn start:android` to start the packager and run the app in the the Android device/emulator (`yarn start:android:logger` will boot the application with [redux-logger](https://github.com/evgenyrodionov/redux-logger))

Please take a look at the [contributing guidelines](./CONTRIBUTING.md) for a detailed process on how to build your application as well as troubleshooting information.

**Development Keys**: The `CLIENT_ID` and `CLIENT_SECRET` in `api/index.js` are for development purposes and do not represent the actual application keys. Feel free to use them or use a new set of keys by creating an [OAuth application](https://github.com/settings/applications/new) of your own. Set the "Authorization callback URL" to `gitpoint://welcome`.


## Acknowledgments

Thanks to [UnionDigital](https://www.unionitdigital.com.br) 
