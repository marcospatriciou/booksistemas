<p align="center">
  <a href="https://gitpoint.co/">
    <img alt="GitPoint" title="GitPoint" src="https://www.jadlog.com.br/jadlog/img/logo_home.png" width="450">
  </a>
</p>

<p align="center">
  BitBucket Corporativo.
</p>



<!-- START doctoc generated TOC please keep comment here to allow auto update -->
<!-- DON'T EDIT THIS SECTION, INSTEAD RE-RUN doctoc TO UPDATE -->
## Sobre o módulo TMS

- [Objetivo](#objetivo)
- [Características](#caracteristicas)
- [Pré-Requisitos](#prerequisitos)
- [Parâmetros](#parametros)
- [Integração](#integracao)


<!-- END doctoc generated TOC please keep comment here to allow auto update -->

## Objetivo


- Efetuar a manutenção e a emissão de toda a documentação necessária - para o transporte de mercadorias.
Contratar e controlar o transporte efetuado por fornecedores terceirizados.
- Permitir o controle da frota de veículos de uma transportadora.
- Permitir o provisionamento de custos com fretes para Indústrias e Operadores Logísticos.


## Caracteristicas

- Flexibilidade na negociação de frete, principalmente com a tabela de frete nova.
- Controle total do transporte de mercadorias, desde a coleta até a entrega.
- Controle da documentação e do transporte efetuado por fornecedores terceirizados.

## PreRequisitos


- Execução dos conversores para criação de tabelas e parâmetros.
- Alimentar os parâmetros com os valores corretos, caso for diferente do valor padrão gerado pelos conversores.
- Efetuar a parametrização do TMS pela rotina JAD_XXXX (Parâmetros TMS), cadastrar os emitentes, tabelas de frete, veículos, entre outras informações.


## Parametros

Os parâmetros são indicadores que permitem ao Sistema um processamento diferenciado de determinadas funções, por meio do uso de uma mesma rotina.

Os parâmetros padrões relacionados a este módulo estão descritos na rotina JAD_XXXX (Central de Parâmetros).

## Integracao

O TMS efetua integrações das seguintes formas:

Para clientes que trabalham na visão Transportador:

Conhecimento de Transporte: são integrados com os módulos de Faturamento, Contas a Receber, Contabilidade, Obrigações Fiscais.
Contratos de Terceiro: são integrados com o módulo de Contas a Pagar .

Para clientes que trabalharem na visão Embarcador:

Nota Fiscal Virtual: é gerada no TMS a partir do faturamento de Notas Fiscais. Também pode ser efetuado a partir de Ordens de Montagem, geradas no módulo de Vendas.
Conhecimentos de Transportadoras: após efetuada a conferência, estes conhecimentos são integrados com os módulos de Suprimentos, Contas a Pagar e Contabilidade.